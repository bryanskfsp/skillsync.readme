# Bryansk Team of FSP Russia

# SkillSync

Синхронизация навыков и требований.
<div align="center">
<p>
<a href="https://ссылка">
<img src="https://i.imgur.com/D8ma4gz.png" alt="Logo">
</a>
</p>
</div>

### Architecture

<div align="center">
<p>
<img src="https://i.imgur.com/pK0k5Uh.png" alt="Architecture">
</a>
</div>

## Getting Started

- [Web site](http://89.104.68.193:8080)
- [Admin site](http://89.104.68.193:8123/admin)
- [Figma](ссылка)

## Sources
- [Core](https://gitlab.com/bryanskfsp/skillsync.net)
- [Core.Swagger](https://prod.skillsync.kaboom.pro/swagger)
- [ML Core](https://gitlab.com/bryanskfsp/ml)
- [Web source](https://gitlab.com/bryanskfsp/skillsync.Front)
- [Admin source](https://gitlab.com/bryanskfsp/skillsync.Python)
- [Telegram bot Sources](https://gitlab.com/bryanskfsp/skillsync.tg)
- [Mobile Sources](https://gitlab.com/bryanskfsp/skillsync.mobile)

## Teams

- [Nikita Shidlovskii, Captain, Fullstack](https://gitlab.com/nlk81)
- [Konstantin Pikatov, Product Manager](https://gitlab.com/pika4uha)
- [Elisey Verevkin, Mobile developer](https://github.com/injent)
